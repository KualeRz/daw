﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;


namespace EmployeeQuiz.Models
{
    public class PayrollDm
    {
        List<Employee> empList;

        public PayrollDm(string dbPath)
        {
            var reader = new StreamReader(File.OpenRead(dbPath));


            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var valores = line.Split(',');
                empList.Add(new Employee
                {
                    Id = valores[0],
                    FirstLastName = valores[1],
                    SecondLastName = valores[2],
                    Name = valores[3],
                    Position = valores[4],
                    Wage = double.Parse(valores[5]),
                    Sex = char.Parse(valores[6]),
                    PhotoPath = valores[7],
                   
                  
                }
                );

                


            }

        }

       public Employee GetEmployeeById(string id)
        {
            var emp = empList.Find(e => e.Id == id);
            return emp;
        }
    }
}