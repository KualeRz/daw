﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pk2_RSVPwapp.Models;

namespace Pk2_RSVPwapp.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            var hour = DateTime.Now.Hour;
            ViewBag.Greeting = hour < 12 ? "Buenos Días" : "Buenas Tardes";
            return View();
        }
        [HttpGet]
        public ViewResult Rsvpform()
        {
            return View();
        }

        [HttpPost]
        public ViewResult Rsvpform(GuestResponse guestResponse)
        {


            //verificando errores de validacion
            if (ModelState.IsValid)
            {

                //Todo: Enviar respuesta al correo del organizador
                return View("Agradecimientos", guestResponse);
            }
            else
            {
                //hay un prioblema de validacion
                return View();

            }
        }
    }
}