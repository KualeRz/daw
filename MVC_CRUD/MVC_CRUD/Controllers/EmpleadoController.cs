﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_CRUD.Controllers
{
    public class EmpleadoController : Controller
    {
        private EmpleadosEntities1 db = new EmpleadosEntities1();

        //
        // GET: /Empleado/

        public ActionResult Index()
        {
            return View(db.empleadoes.ToList());
        }

        //
        // GET: /Empleado/Details/5

        public ActionResult Details(int id_empleado = 0)
        {
            empleado empleado = db.empleadoes.Find(id_empleado);
            if (empleado == null)
            {
                return HttpNotFound();
            }
            return View(empleado);
        }

        //
        // GET: /Empleado/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Empleado/Create

        [HttpPost]
        public ActionResult Create(empleado empleado)
        {
            if (ModelState.IsValid)
            {
                db.empleadoes.Add(empleado);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(empleado);
        }

        //
        // GET: /Empleado/Edit/5

        public ActionResult Edit(int id_empleado = 0)
        {
            empleado empleado = db.empleadoes.Find(id_empleado);
            if (empleado == null)
            {
                return HttpNotFound();
            }
            return View(empleado);
        }

        //
        // POST: /Empleado/Edit/5

        [HttpPost]
        public ActionResult Edit(empleado empleado)
        {
            if (ModelState.IsValid)
            {
                db.Entry(empleado).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(empleado);
        }

        //
        // GET: /Empleado/Delete/5

        public ActionResult Delete(int id_empleado = 0)
        {
            empleado empleado = db.empleadoes.Find(id_empleado);
            if (empleado == null)
            {
                return HttpNotFound();
            }
            return View(empleado);
        }

        //
        // POST: /Empleado/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id_empleado)
        {
            empleado empleado = db.empleadoes.Find(id_empleado);
            db.empleadoes.Remove(empleado);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}