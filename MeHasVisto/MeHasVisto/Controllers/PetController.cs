﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeHasVisto.Controllers
{
    public class PetController : Controller
    {
        //
        // GET: /Pet/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Display()
        {
            var name = (string)RouteData.Values["id"];
            //var controller = (string)RouteData.Values["controller"];
           //var action = (string)RouteData.Values["action"];
           object model = null;
           if (model == null)
                return RedirectToAction("NotFound");
            return View(model);
            
        }

        public FileResult DownloadPicture()
        {
            var name = (string)RouteData.Values["id"];
            var picture = "/Content/Uploads/" + name + ".jpg";
            var contentType = "image/jpg";
            var fileName = name + ".jpg";
            return File(picture, contentType, fileName);
                 
        }

        public ActionResult NotFound()
        {
          ViewBag.ErrorCode ="NFE0001";
          ViewBag.Description = "La mascota no se encuentra" 
              + "en la base de datos";
          return View();

        }

    }
}
