﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Web;

namespace MeHasVisto.Models.Bussiness
{
    public class PetManagement
    {
        public static void CreateThumbNail(
            string fileName, string filePath,
            int thumbWi, int thumbHi,
            bool maintainAspect)
        {
            var originalFile = Path.Combine(filePath, fileName);
            var source = Image.FromFile(originalFile);
            if (source.Width <= thumbWi &&
                source.Height <= thumbHi)
                return;
        }

    }
}