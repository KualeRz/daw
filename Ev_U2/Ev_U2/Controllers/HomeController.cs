﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ev_U2.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        public ViewResult ConsultaEmpleado()
        {
            return View();
        }

        public ViewResult VistaEmpleado()
        {
            int number = Convert.ToInt32(Request["Control"]);

            EmpleadosDataContext dc = new EmpleadosDataContext();
            empleado empleado = dc.empleado.FirstOrDefault(a => a.id_empleado == number);

            ViewData["empleado"] = empleado;

            return View();
        }

    }
}
